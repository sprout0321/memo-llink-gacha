import Vue from 'vue';
import Login from './login.vue';
import VueRouter from 'vue-router';
import Main from './main.vue';
import BoostrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
const routes = [
    { path: '/main', component: Main },
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes
});

Vue.use(VueRouter);
Vue.use(BoostrapVue);

const app = new Vue({
    el: "#login",
    router: router,
    components: {Login},
    template: "<Login/>"
});