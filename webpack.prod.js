const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    plugins: [
        new webpack.DefinePlugin({
            'SERVICE_URL': JSON.stringify('alita-client.herokuapp.com')
        }),
        new UglifyJSPlugin({
            sourceMap: true
        })
    ]
});